//
//

import Foundation


func getAllTabsData()->NSArray
{
    return [
        [
            "category":"TAB-1",
            "image":"ictab1",
            "data":[
                [
                    "section":"Bang Bang",
                    "data": [
                                [
                                    "title": "Bang Bang!",
                                    "subtitle": "Guns fire explosive bullets.",
                                    "image": "icbtncright,icbtna,icbtnx,icbtncleft,icbtnrb,icbtna,icbtny,icbtnrb,icbtnb,icbtnb,icbtnrt,icbtncleft,icbtncright,icbtncright,icbtnlb,icbtnlb,icbtnlb"
                                ],
                                [
                                    "title": "Flaming Bullets",
                                    "subtitle": "Your bullets will set things on fire upon impact.",
                                    "image": "icbtnlb,icbtnrb,icbtnx,icbtnrb,icbtncleft,icbtna,icbtny,icbtnrb,icbtnb,icbtnb,icbtnrt,icbtnrb,icbtncleft,icbtnx,icbtncright,icbtnlb,icbtnlb"
                                ],
                                [
                                    "title": "Explosive Melee Attacks",
                                    "subtitle": "Your punches and kicks are literally explosive!",
                                    "image": "icbtncright,icbtncleft,icbtna,icbtny,icbtnrb,icbtnb,icbtnb,icbtnb,icbtnlt"
                                ],
                                [
                                    "title": "Super Jump",
                                    "subtitle": "Hold jump to go even higher.",
                                    "image": "icbtncleft,icbtncleft,icbtny,icbtny,icbtncright,icbtncright,icbtncleft,icbtncright,icbtnx,icbtnrb,icbtnrt"
                                ],
                                [
                                    "title": "Drunk Mode",
                                    "subtitle": "Increases your maximum swimming speed",
                                    "image": "icbtny,icbtncright,icbtncright,icbtncleft,icbtna,icbtny,icbtnrb,icbtnb,icbtnb,icbtncright,icbtnx,icbtnb,icbtncleft"
                                ],
                                [
                                    "title": "Fast Run",
                                    "subtitle": "Increases your maximum sprint speed.",
                                    "image": "icbtny,icbtncleft,icbtncright,icbtncright,icbtnlt,icbtnlb,icbtnx"
                                ],
                                [
                                    "title": "Fast Swim",
                                    "subtitle": "Increases your maximum swimming speed",
                                    "image": "icbtncleft,icbtncleft,icbtnlb,icbtncright,icbtna,icbtny,icbtnrb,icbtnb,icbtnb,icbtncright,icbtnrt,icbtncleft,icbtnlt,icbtncright"
                                ],
                                [
                                    "title": "Slow Motion Aiming",
                                    "subtitle": "Enter 3x for increased effect, 4x disables it.",
                                    "image": "icbtnx,icbtnlt,icbtnrb,icbtny,icbtncleft,icbtnx,icbtnlt,icbtncright,icbtna"
                                ]
                        ]
                ],
                [
                    "section":"Slidey Cars",
                    "data": [
                        [
                          "title": "Change Weather",
                          "subtitle": "Change Weather Cycles through the following 9 weather types: Extra sunny, clear, cloudy, smoggy, overcast, rainy, thundery, clearing, snowy.",
                          "image": "icbtnrt,icbtna,icbtnlb,icbtnlb,icbtnlt,icbtnlt,icbtnlt,icbtnx"
                        ],
                        [
                          "title": "Slidey Cars",
                          "subtitle": "Also known as drifting.",
                          "image": "icbtny,icbtnrb,icbtnrb,icbtncleft,icbtnrb,icbtnlb,icbtnrt,icbtnlb"
                        ],
                        [
                          "title": "Slow Motion",
                            "subtitle": "Enter a vehicle to experience less gravity",
                            "image": "icbtny,icbtnrb,icbtnrb,icbtncleft,icbtnrb,icbtnlb,icbtnrt,icbtnlb"
                        ],
                        [
                          "title": "Moon Gravity",
                          "subtitle": "Enter a vehicle to experience less gravity",
                          "image": "icbtncleft,icbtncleft,icbtnlb,icbtnrb,icbtnlb,icbtncright,icbtncleft,icbtnlb,icbtncleft"
                        ]
                      ]
                ],
                [
                    "section":"BMX bicycle",
                    "data": [
                        [
                          "title": "Spawn BMX",
                          "subtitle": "Yup, a BMX bicycle.",
                          "image": "icbtncleft,icbtncleft,icbtncright,icbtncright,icbtncleft,icbtncright,icbtnx,icbtnb,icbtny,icbtnrb,icbtnrt"
                        ],
                        [
                          "title": "Spawn Buzzard",
                          "subtitle": "Small attack helicopter.",
                          "image": "icbtnb,icbtnb,icbtnlb,icbtnb,icbtnb,icbtnb,icbtnlb,icbtnlt,icbtnrb,icbtny,icbtnb,icbtny"
                        ],
                        [
                          "title": "Spawn Caddy",
                          "subtitle": "Golf cart.",
                          "image": "icbtnb,icbtnlb,icbtncleft,icbtnrb,icbtnlt,icbtna,icbtnrb,icbtnlb,icbtnb,icbtna"
                        ],
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Two door sports car.",
                          "image": "icbtnrb,icbtnb,icbtnrt,icbtncright,icbtnlb,icbtnlt,icbtna,icbtna,icbtnx,icbtnrb"
                        ],
                        [
                          "title": "Spawn Duster",
                          "subtitle": "Crop duster plane.",
                          "image": "icbtncright,icbtncleft,icbtnrb,icbtnrb,icbtnrb,icbtncleft,icbtny,icbtny,icbtna,icbtnb,icbtnlb,icbtnlb"
                        ],
                        [
                            "title": "Spawn Duster",
                          "subtitle": "Dual-sport (dirt) bike.",
                          "image": "icbtnb,icbtna,icbtnlb,icbtnb,icbtnb,icbtnlb,icbtnb,icbtnrb,icbtnrt,icbtnlt,icbtnlb,icbtnlb"
                        ],
                        [
                            "title": "Spawn Duster",
                          "subtitle": "Fixed-wing stunt plane.",
                          "image": "icbtnb,icbtncright,icbtnlb,icbtnlt,icbtncleft,icbtnrb,icbtnlb,icbtnlb,icbtncleft,icbtncleft,icbtna,icbtny"
                        ],
                        [
                            "title": "Spawn Duster",
                          "subtitle": "Garbage truck.",
                          "image": "icbtnb,icbtnrb,icbtnb,icbtnrb,icbtncleft,icbtncleft,icbtnrb,icbtnlb,icbtnb,icbtncright"
                        ]
                      ]
                ],
                [
                    "section":"SPECIAL VEHICLES",
                    "data": [
                        [
                          "title": "Spawn Dodo",
                          "subtitle": "Unlock by completing the 'Sea Plane' random event.",
                          "number": "1-999-398-3232 (EXTINCT)"
                        ],
                        [
                          "title": "Spawn Duke O'Death",
                          "subtitle": "Unlock by completing the 'Duel' random event.",
                          "number": "1-999-444-4227 (DEATHCAR)"
                        ],
                        [
                          "title": "Spawn Kraken",
                          "subtitle": "Unlock by completing the 'Wildlife Photography Challenge'.",
                          "number": "1-3344-282-2537 (BUBBLES)"
                        ]
                      ]
                ]
            ]
        ],
        [
            "category":"TAB-2",
            "image":"ictab2",
            "data":[
                [
                    "section":"Invincibility",
                    "data":[
                        [
                          "title": "Give Weapons",
                          "subtitle": "Gives you all the weapons along with ammo!",
                          "image": "icbtntriangle,icbtnr2,icbtnbleft,icbtnl1,icbtncross,icbtnbright,icbtntriangle,icbtnbdown,icbtnsquare,icbtnl1,icbtnl1,icbtnl1"
                        ],
                        [
                          "title": "Max Health Invincibility+ Armor",
                            "subtitle": "Increases your maximum swimming speed",
                          "image": "icbtncircle,icbtnl1,icbtntriangle,icbtnr2,icbtncross,icbtnsquare,icbtncircle,icbtnbright,icbtnr2,icbtncross,icbtnsquare,icbtncircle,icbtnbright,icbtnr2,icbtncross,icbtnsquare,icbtncircle,icbtnbright,icbtnsquare,icbtnl1,icbtnl1,icbtnl1"
                        ],
                        [
                          "title": "Invincibility",
                          "subtitle": "Works for 5 minutes.",
                          "image": "icbtnbright,icbtncross,icbtnbright,icbtnbleft,icbtnbright,icbtnr1,icbtnbright,icbtnbleft,icbtncross,icbtntriangle"
                        ],
                        [
                          "title": "Fast Swim",
                          "subtitle": "Increases your maximum swimming speed",
                          "image": "icbtnbleft,icbtnbleft,icbtnl1,icbtnbright,icbtnbright,icbtnr2,icbtnbleft,icbtnl2,icbtnbright"
                        ],
                        [
                          "title": "Slow Invincibility",
                            "subtitle": "Increases your maximum swimming speed",
                          "image": "icbtnsquare,icbtnl2,icbtnr1,icbtntriangle,icbtnbleft,icbtnsquare,icbtnl2,icbtnbright,icbtncross"
                        ]
                    ]
                ],
                [
                    "section":"WORLD EFFECTS",
                    "data":[
                        [
                          "title": "Slow Motion",
                          "subtitle": "Enter 3x for increased effect, 4x disables it.",
                          "image": "icbtntriangle,icbtnbleft,icbtnbright,icbtnbright,icbtnsquare,icbtnr2,icbtnr1"
                        ],
                        [
                          "title": "Moon Gravity",
                          "subtitle": "Enter a vehicle to experience less gravity",
                          "image": "icbtnbleft,icbtnbleft,icbtnl1,icbtnr1,icbtnl1,icbtnbright,icbtnbleft,icbtnl1,icbtnbleft"
                        ]
                    ]
                ],
                [
                    "section":"BMX bicycle",
                    "data":[
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Yup, a BMX bicycle.",
                          "image": "icbtnbleft,icbtnbleft,icbtnbright,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnbright,icbtnbleft,icbtnbright,icbtnsquare,icbtncircle,icbtntriangle,icbtnr1,icbtnr2"
                        ],
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Small attack helicopter.",
                          "image": "icbtncircle,icbtncircle,icbtnl1,icbtncircle,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtncircle,icbtncircle,icbtnl1,icbtnl2,icbtnr1,icbtntriangle,icbtncircle,icbtntriangle"
                        ],
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Golf cart.",
                          "image": "icbtncircle,icbtnl1,icbtnbleft,icbtnr1,icbtnl2,icbtncross,icbtnr2,icbtnl1,icbtncircle,icbtncross"
                        ],
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Two door sports car.",
                          "image": "icbtnr1,icbtncircle,icbtnr2,icbtnbright,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnl1,icbtnl2,icbtncross,icbtncross,icbtnsquare,icbtnr1"
                        ],
                        [
                          "title": "Spawn Comet",
                          "subtitle": "Crop duster plane.",
                          "image": "icbtnbright,icbtnbleft,icbtnr1,icbtnr1,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnr1,icbtnbleft,icbtntriangle,icbtntriangle,icbtncross,icbtncircle,icbtnl1,icbtnl1"
                        ],
                        [
                          "title": "Spawn Limo",
                          "subtitle": "Stretch limousine.",
                          "image": "icbtnr1,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnr1,icbtnl1,icbtncircle,icbtnbright"
                        ],
                        [
                          "title": "Spawn PCJ",
                          "subtitle": "Sports motorbike.",
                          "image": "icbtnr1,icbtnbright,icbtnbleft,icbtnbright,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnr2,icbtnbleft,icbtnbright,icbtnsquare,icbtnbright,icbtnl2,icbtnl1,icbtnl1"
                        ],
                        [
                            "title": "Spawn PCJ",
                          "subtitle": "Two door sports car",
                            "image": "icbtnr1,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnr1,icbtnl1,icbtncircle,icbtnbright"
                        ],
                        [
                          "title": "Spawn Sanchez",
                          "subtitle": "Dual-sport (dirt) bike.",
                          "image": "icbtncircle,icbtncross,icbtnl1,icbtncircle,icbtncircle,icbtnl1,icbtncircle,icbtnr1,icbtnr2,icbtnl2,icbtnl1,icbtnl1"
                        ],
                        [
                          "title": "Spawn Stunt Plane",
                          "subtitle": "Fixed-wing stunt plane.",
                          "image": "icbtncircle,icbtnbright,icbtnl1,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnl2,icbtnbleft,icbtnr1,icbtnl1,icbtnl1,icbtnbleft,icbtnbleft,icbtncross,icbtntriangle"
                        ],
                        [
                          "title": "Spawn Trashmaster",
                          "subtitle": "Garbage truck.",
                          "image": "icbtncircle,icbtnr1,icbtncircle,icbtnbright,icbtnl2,icbtnbleft,icbtnbleft,icbtnr1,icbtnbleft,icbtnbleft,icbtnr1,icbtnl1,icbtncircle,icbtnbright"
                        ]
                    ]
                ],
                [
                    "section":"Spawn Duke",
                    "data":[
                        [
                          "title": "Spawn Dodo",
                          "subtitle": "Unlock by completing the \"Sea Plane\" random event.",
                          "number": "1-222-232-4628 (completing)"
                        ],
                        [
                          "title": "Spawn Duke O'Death",
                          "subtitle": "Unlock by completing the \"Duel\" random event.",
                            "number": "1-222-232-4628 (completing)"
                        ],
                        [
                            "title": "Spawn Duke O'Death",
                          "subtitle": "Unlock by completing the \"Wildlife Photography Challenge\".",
                            "number": "1-222-232-4628 (completing)"
                        ]
                    ]
                ]
            ]
        ],
        [
            "category":"TAB-3",
            "image":"ictab3",
            "data":[
                [
                    "section":"Weapons Aiming",
                    "data":[
                        [
                          "title": "Give Weapons",
                          "subtitle": "Gives you all the weapons along with ammo!",
                          "number": "1-999-8665-87 (TOOLUP)"
                        ],
                        [
                          "title": "Lower Wanted Level",
                          "subtitle": "Decreases wanted level by one star.",
                            "number": "1-999-8665-87 (TOOLUP)"
                        ],
                        [
                          "title": "Slow Motion Aiming",
                          "subtitle": "Enter 3x for increased effect, 4x disables it.",
                          "number": "1-999-8665-87 (TOOLUP)"
                        ]
                    ]
                ],
                [
                    "section":"Weather Gravity",
                    "data":[
                        [
                          "title": "Change Weather",
                          "subtitle": "Cycles through the following 9 weather types: Extra sunny, clear, cloudy,sunny, clear, cloudy,sunny, clear, cloudy,sunny, clear, cloudy, smoggy, overcast, rainy, thundery, clearing, snowy.",
                          "number": "1-999-625-348-7246 (MAKEITRAIN)"
                        ],
                        [
                          "title": "Moon Gravity",
                          "subtitle": "Enter a vehicle to experience less gravity",
                          "number": "1-999-356-2837 (FLOATER)"
                        ]
                    ]
                ],
                [
                    "section":"Sanchez",
                    "data":[
                        [
                          "title": "Spawn BMX",
                          "subtitle": "Yup, a BMX bicycle.",
                          "number": "1-2323-3434-348 (BANDIT)"
                        ],
                        [
                          "title": "Sanchez Buzzard",
                          "subtitle": "Small attack helicopter.",
                          "number": "1-999-223-9633 (BUZZOFF)"
                        ],
                        [
                          "title": "Spawn Sanchez",
                          "subtitle": "Dual-sport (dirt) bike.",
                          "number": "1-999-633-5633 (OFFROAD)"
                        ],
                        [
                          "title": "Sanchez Stunt Plane",
                          "subtitle": "Fixed-wing stunt plane.",
                          "number": "1-999-2276-78676 (BARNSTORM)"
                        ],
                        [
                          "title": "Sanchez Trashmaster",
                          "subtitle": "Garbage truck.",
                          "number": "1-999-872-433 (TRASHED)"
                        ]
                    ]
                ],
                [
                    "section":"Spawn Duke",
                    "data":[
                        [
                          "title": "Spawn Duke completing",
                          "subtitle": "Unlock ing the \"Duel\" random event.",
                          "number": "1-999-3434-4227 (DEATHCAR)"
                        ],
                        [
                          "title": "Spawn Kraken",
                          "subtitle": "Unlock by completing the \"Wildlife Photography Challenge\".",
                          "number": "1-999-3434-2537 (BUBBLES)"
                        ]
                    ]
                ]
            ]
        ]
    ]
}



