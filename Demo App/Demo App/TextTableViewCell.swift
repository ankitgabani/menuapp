//
//  TextTableViewCell.swift
//  Demo App
//
//  Created by Mac MIni M1 on 21/07/21.
//

import UIKit

class TextTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblTitlr: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
