//
//  ViewController.swift
//  Demo App
//
//  Created by Mac MIni M1 on 21/07/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet var viewBannerBase : UIView!
    @IBOutlet var consBannerHeight : NSLayoutConstraint!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    var arrMenu = NSMutableArray()
    var arrData = NSMutableArray()
    
    
    //MARK:- View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       

        tblView.delegate = self
        tblView.dataSource = self
        
        let dicData = getAllTabsData()[0] as? NSDictionary
        let arrData = dicData?.value(forKey: "data") as? NSArray
        arrMenu = (arrData?.mutableCopy() as? NSMutableArray)!
        
        setUpTriangle(objView: view1)
        view1.isHidden = false
        view2.isHidden = true
        view3.isHidden = true
        
        if UIDevice.current.hasNotch
        {
            if #available(iOS 11.0, *) {
                let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                print(bottom)
                consBannerHeight.constant = 50.0 + bottom
            }
        }
        else
        {
            consBannerHeight.constant = 50
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Action Method
    @IBAction func clickedTab_1(_ sender: Any) {
        
        setUpTriangle(objView: view1)
        view1.isHidden = false
        view2.isHidden = true
        view3.isHidden = true
        
        let dicData = getAllTabsData()[0] as? NSDictionary
        let arrData = dicData?.value(forKey: "data") as? NSArray
        arrMenu = (arrData?.mutableCopy() as? NSMutableArray)!
        
        let indexPath = NSIndexPath(row: 0, section: 0)
        
        self.tblView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
        self.tblView.reloadData()
    }
    
    @IBAction func clickedTab_2(_ sender: Any) {
        
        setUpTriangle(objView: view2)
        view1.isHidden = true
        view2.isHidden = false
        view3.isHidden = true
        
        let dicData = getAllTabsData()[1] as? NSDictionary
        let arrData = dicData?.value(forKey: "data") as? NSArray
        arrMenu = (arrData?.mutableCopy() as? NSMutableArray)!
        
        let indexPath = NSIndexPath(row: 0, section: 0)
        
        self.tblView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
        self.tblView.reloadData()
        
    }
    
    @IBAction func clicked3(_ sender: Any) {
        
        setUpTriangle(objView: view3)
        view1.isHidden = true
        view2.isHidden = true
        view3.isHidden = false
        
        let dicData = getAllTabsData()[2] as? NSDictionary
        let arrData = dicData?.value(forKey: "data") as? NSArray
        arrMenu = (arrData?.mutableCopy() as? NSMutableArray)!
        
        let indexPath = NSIndexPath(row: 0, section: 0)
        
        self.tblView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
        self.tblView.reloadData()
        
    }
    
    
    //MARK:- table View Delegate And Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrData = (arrMenu[section] as? NSDictionary)?.value(forKey: "data") as? NSArray
        return arrData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let arrData = (arrMenu[indexPath.section] as? NSDictionary)?.value(forKey: "data") as? NSArray
        
        let dicData = arrData?[indexPath.row] as? NSDictionary
        
        let title = dicData?.value(forKey: "title") as? String
        let subtitle = dicData?.value(forKey: "subtitle") as? String
        let number = dicData?.value(forKey: "number") as? String
        let image1 = dicData?.value(forKey: "image") as? String
        
        let sectionInsetsSlider = UIEdgeInsets(top: 0.0,
                                               left: 0.0,
                                               bottom: 0.0,
                                               right: 0.0)
        
        var flowLayoutSlider: UICollectionViewFlowLayout {
            let _flowLayout = UICollectionViewFlowLayout()
            
            _flowLayout.itemSize = CGSize(width: 40, height: 40)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _flowLayout.minimumInteritemSpacing = 0
            _flowLayout.minimumLineSpacing = 0
            return _flowLayout
        }
        
        if number == "" ||  number == nil
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ImageTableViewCell") as! ImageTableViewCell
            
            cell.lblTitle.text = title
            cell.lblSubTitle.text = subtitle
            
            let array = image1?.components(separatedBy: ",")
            cell.collectionView1.delegate = self
            cell.collectionView1.dataSource = self
            cell.collectionView1.collectionViewLayout = flowLayoutSlider
            cell.collectionView1.accessibilityElements = [array]
            
            cell.collectionView1.reloadData()
            
            cell.collectionViewHieghtcont.constant = cell.collectionView1.collectionViewLayout.collectionViewContentSize.height
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "TextTableViewCell") as! TextTableViewCell
            
            cell.lblTitlr.text = title
            cell.lblSubTitle.text = subtitle
            cell.lblNumber.text = number
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let arrData = (arrMenu[indexPath.section] as? NSDictionary)?.value(forKey: "data") as? NSArray
        
        let dicData = arrData?[indexPath.row] as? NSDictionary
        
        let number = dicData?.value(forKey: "number") as? String
        
        if number == "" ||  number == nil
        {
            return UITableView.automaticDimension
        }
        else
        {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HeaderView", owner: self, options: [:])?.first as! HeaderView
        
        let strTitle = (arrMenu[section] as? NSDictionary)?.value(forKey: "section") as? String
        headerView.lblTitle.text = strTitle?.uppercased()
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //MARK:- Collection View Delegate And Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.accessibilityElements!.count > 0{
            
            let arrImage = collectionView.accessibilityElements?.first as! [String]
            
            return arrImage.count
        }
        else{
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell
        let arrImage = collectionView.accessibilityElements?.first as! [String]
        
        cell?.imgPro.image = UIImage(named: arrImage[indexPath.row] as! String)
        
        return cell!
    }
    
    func setUpTriangle(objView: UIView){
        let heightWidth = objView.frame.size.width
        let path = CGMutablePath()
        
        path.move(to: CGPoint(x: 0, y: heightWidth))
        path.addLine(to: CGPoint(x:heightWidth/2, y: heightWidth/2))
        path.addLine(to: CGPoint(x:heightWidth, y:heightWidth))
        path.addLine(to: CGPoint(x:0, y:heightWidth))
        
        let shape = CAShapeLayer()
        shape.path = path
        shape.fillColor = UIColor.black.cgColor
        
        objView.layer.insertSublayer(shape, at: 0)
    }
}

extension UIDevice {
    var hasNotch: Bool
    {
        if #available(iOS 11.0, *)
        {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else
        {
            // Fallback on earlier versions
            return false
        }
    }
}
