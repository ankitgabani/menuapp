//
//  FavoritesViewController.swift
//  Menu App
//
//  Created by Gabani King on 19/07/21.
//

import UIKit
import SDWebImage

class FavoritesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    @IBOutlet var viewBannerBase : UIView!
    @IBOutlet var consBannerHeight : NSLayoutConstraint!
    
    var arrMenu = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrMenu = (DataManger.getFavourites("").mutableCopy() as? NSMutableArray)!
        self.tblView.reloadData()
        
        if arrMenu.count > 0
        {
            viewNoData.isHidden = true
        }
        else
        {
            viewNoData.isHidden = false
        }
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK- Action Method
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
    }
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK- UITableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecretMenuCell") as! SecretMenuCell
        
        var dicData = NSDictionary()
        
        dicData = arrMenu[indexPath.row] as! NSDictionary
        
        let title = dicData.value(forKey: "title") as? String
        cell.lblMenu.text = title
        
        var picture = dicData.value(forKey: "picture") as? String
        picture = picture?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "https://appcodism.com/jamesapps/menu/\(picture ?? "")")
        cell.imgMenu.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgMenu.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dicData = NSDictionary()
        dicData = arrMenu[indexPath.row] as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailScreenViewController") as! DetailScreenViewController
        vc.dicDetails = dicData
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
}
