//
//  AppDelegate.swift
//  Menu App
//
//  Created by Gabani King on 19/07/21.
//

import UIKit
import IQKeyboardManager
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Handle user allowing / declining notification permission. Example:
            if (granted) {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            } else {
                print("User declined notification permissions")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            // UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            // center.delegate  = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
        else{
            
            let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
            
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        
        // Override point for customization after application launch.
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": ""]) // Notification
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        let objUpdateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
        objUpdateVC.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(objUpdateVC, animated: true, completion: nil)
        
        completionHandler(.alert)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       
        let objUpdateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
        objUpdateVC.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(objUpdateVC, animated: true, completion: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
            let objUpdateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
            objUpdateVC.modalPresentationStyle = .fullScreen
            UIApplication.topViewController()?.present(objUpdateVC, animated: true, completion: nil)        
        
        completionHandler()
    }
    
    func setUpLocalNotification(hour: Int, minute: Int) {
        let content = UNMutableNotificationContent()
        content.title = "Menu App"
        content.body = "Please checking for gift card"
        content.sound = UNNotificationSound.default
        var dateComponents = DateComponents()
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)
        
        let triggerDaily = Calendar.current.dateComponents([.hour, .minute, .second], from: someDateTime!)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        
        //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: triggerDaily, repeats: true)
        
        let request = UNNotificationRequest(identifier: "TestIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.getPendingNotificationRequests(completionHandler: { requests in
            for request in requests {
                print(request)
            }
        })
        
    }


}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
