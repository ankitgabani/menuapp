//
//  DetailScreenViewController.swift
//  Menu App
//
//  Created by Mac MIni M1 on 21/07/21.
//

import UIKit
import SDWebImage

class DetailScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet var viewBannerBase : UIView!
    @IBOutlet var consBannerHeight : NSLayoutConstraint!
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnCloseZZ: UIButton!
    
    @IBOutlet weak var TBLVIEW: UITableView!
    
    let imageView = UIImageView()
    var dicDetails = NSDictionary()
    
    var arrIntro = NSMutableArray()
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnCloseC = UIButton.init(type: .custom)
        
        TBLVIEW.estimatedRowHeight = 50
        TBLVIEW.contentInset = UIEdgeInsets(top: 270, left: 0, bottom: 0, right: 0)
        
        TBLVIEW.delegate = self
        TBLVIEW.dataSource = self
        
        if UIDevice.current.hasNotch
        {
            if #available(iOS 11.0, *) {
                let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                print(bottom)
                consBannerHeight.constant = 50.0 + bottom
            }
        }
        else
        {
            consBannerHeight.constant = 50
        }
        
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.groupTableViewBackground
        
        if #available(iOS 11.0, *) {
            let topSAfe = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            imageView.frame = CGRect(x: 0, y: topSAfe, width: UIScreen.main.bounds.size.width, height: 270)
            
            btnCloseC.frame = CGRect(x: 18, y: 13, width: 35, height: 35)
            btnCloseC.addTarget(self, action: #selector(clickedCancel), for: .touchUpInside)
            btnCloseC.setImage(UIImage(named: "icclose"), for: .normal)
            imageView.addSubview(btnCloseC)
            
            var picture = dicDetails.value(forKey: "picture") as? String
            picture = picture?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "https://appcodism.com/jamesapps/menu/\(picture ?? "")")
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            imageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            view.addSubview(imageView)
            
        } else {
            imageView.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width, height: 270)
            
            
            btnCloseC.frame = CGRect(x: 18, y: 13, width: 35, height: 35)
            btnCloseC.addTarget(self, action: #selector(clickedCancel), for: .touchUpInside)
            btnCloseC.setImage(UIImage(named: "icclose"), for: .normal)
            imageView.addSubview(btnCloseC)
            
            var picture = dicDetails.value(forKey: "picture") as? String
            picture = picture?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: "https://appcodism.com/jamesapps/menu/\(picture ?? "")")
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            imageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            view.addSubview(imageView)
        }
        
        
        let strIntro = dicDetails.value(forKey: "recipe") as? String
        
        let lines = strIntro?.split(whereSeparator: \.isNewline)
        
        self.arrIntro = ((lines! as NSArray).mutableCopy() as? NSMutableArray)!
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 270 - (scrollView.contentOffset.y + 270)
        let height = min(max(y, 60), 400)
        
        if #available(iOS 11.0, *) {
            let topSAfe = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            imageView.frame = CGRect(x: 0, y: topSAfe, width: UIScreen.main.bounds.size.width, height: height)
        }
        else
        {
            imageView.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width, height: height)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if DataManger.isFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "") == true
        {
            btnFav.setTitle("Unfavorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar1"), for: .normal)
        }
        else
        {
            btnFav.setTitle("Favorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar0"), for: .normal)
        }
    }
    
    //MARK:- Action MEthod
    
    @objc func clickedCancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func clickedFav(_ sender: Any) {
        
        if DataManger.isFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "") == true
        {
            DataManger.removeFromFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "")
            btnFav.setTitle("Favorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar0"), for: .normal)
        }
        else
        {
            DataManger.addToFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", strTitle: dicDetails.value(forKey: "title") as? String ?? "", strCategory: dicDetails.value(forKey: "category") as? String ?? "", strPicture: dicDetails.value(forKey: "picture") as? String ?? "", strRecipe: dicDetails.value(forKey: "recipe") as? String ?? "", strSubmittedBy: dicDetails.value(forKey: "submittedby") as? String ?? "", type: dicDetails.value(forKey: "") as? String ?? "")
            btnFav.setTitle("Unfavorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar1"), for: .normal)
            
        }
        
    }
    
    
    @IBAction func clickedShare(_ sender: Any) {
        
        let url = URL(string: "https://apps.apple.com/us/app/id1554830684")!
        
        let strIntro = dicDetails.value(forKey: "recipe") as? String
        let title = dicDetails.value(forKey: "title") as? String
        
        let strDetails = "Recipe name:- \(title ?? "")\nInstruction:- \n\(strIntro ?? "")\n\n"
        
        let vc = UIActivityViewController(activityItems: [strDetails, url], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        self.present(vc, animated: true)
        
    }
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else
        {
            return arrIntro.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = TBLVIEW.dequeueReusableCell(withIdentifier: "IntroTableViewCell") as! IntroTableViewCell
            
            cell.lblTitle.text = dicDetails.value(forKey: "title") as? String
            
            let submittedby = dicDetails.value(forKey: "submittedby") as? String
            
            if submittedby != ""
            {
                cell.lblSubTitle.text = submittedby
            }
            else
            {
                cell.lblSubTitle.text = ""
            }
            
            return cell
        }
        else
        {
            let cell = TBLVIEW.dequeueReusableCell(withIdentifier: "DetailsScreenTableCell") as! DetailsScreenTableCell
            
            let dicData = arrIntro[indexPath.row]
            
            cell.lblTitle.text = dicData as? String
            
            cell.lblNumber.text = "\(indexPath.row+1)"
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
}
