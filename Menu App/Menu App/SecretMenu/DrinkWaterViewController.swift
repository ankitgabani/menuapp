//
//  DrinkWaterViewController.swift
//  Menu App
//
//  Created by Gabani King on 21/07/21.
//

import UIKit
import SDWebImage

class DrinkWaterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- IBOutlet
    @IBOutlet var viewBannerBase : UIView!
    @IBOutlet var consBannerHeight : NSLayoutConstraint!
    @IBOutlet weak var lblSubmitted: UILabel!
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var TBLVIEW: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    
    var dicDetails = NSDictionary()
    
    var arrIntro = NSMutableArray()
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        if UIDevice.current.hasNotch
                {
                    if #available(iOS 11.0, *) {
                        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                        print(bottom)
                        consBannerHeight.constant = 50.0 + bottom
                    }
                }
                else
                {
                    consBannerHeight.constant = 50
                }
        
        let arrWeekDates = Date().getWeekDates() // Get dates of Current and Next week.
        let dateFormat = "MMM dd" // Date format
        let thisMon = arrWeekDates.thisWeek.first!.toDate(format: dateFormat)
        let thisSun = arrWeekDates.thisWeek[arrWeekDates.thisWeek.count - 1].toDate(format: dateFormat)
        
        let currentWeek = "\(thisMon) - \(thisSun)"
        
        if currentWeek == UserDefaults.standard.value(forKey: "CurrentWeek") as? String
        {
            dicDetails = getCurrentUserData()
        }
        else
        {
            
            let arrMenu = (getMenuData().mutableCopy() as? NSMutableArray)!
            
            let index: Int = Int(arc4random_uniform(UInt32(arrMenu.count)))
            
            let randomVal = arrMenu[index] as! NSDictionary
            dicDetails = randomVal
            
            saveCurrentUserData(dic: dicDetails)
            
            UserDefaults.standard.setValue(currentWeek, forKey: "CurrentWeek")
            UserDefaults.standard.synchronize()
        }
        
        TBLVIEW.delegate = self
        TBLVIEW.dataSource = self
        
        lblTitle.text = dicDetails.value(forKey: "title") as? String
                
        let submittedby = dicDetails.value(forKey: "submittedby") as? String
        
        if submittedby != ""
        {
            lblSubmitted.text = submittedby
        }
        else
        {
            lblSubmitted.text = ""
        }
        
        var picture = dicDetails.value(forKey: "picture") as? String
        picture = picture?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "https://appcodism.com/jamesapps/menu/\(picture ?? "")")
        imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.white
        imgProduct.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        let strIntro = dicDetails.value(forKey: "recipe") as? String
        
        let lines = strIntro?.split(whereSeparator: \.isNewline)
        
        self.arrIntro = ((lines! as NSArray).mutableCopy() as? NSMutableArray)!
        
        // Do any additional setup after loading the view.
    }
    
    func saveCurrentUserData(dic: NSDictionary)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentWeekData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> NSDictionary
    {
        if let data = UserDefaults.standard.object(forKey: "currentWeekData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! NSDictionary
        }
        return NSDictionary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if DataManger.isFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "") == true
        {
            btnFav.setTitle("Unfavorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar1"), for: .normal)
        }
        else
        {
            btnFav.setTitle("Favorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar0"), for: .normal)
        }
    }
    
    //MARK:- Action Method
    @IBAction func clickedFav(_ sender: Any) {
        
        if DataManger.isFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "") == true
        {
            DataManger.removeFromFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", type: "")
            btnFav.setTitle("Favorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar0"), for: .normal)
        }
        else
        {
            DataManger.addToFavourite(dicDetails.value(forKey: "menuid") as? String ?? "", strTitle: dicDetails.value(forKey: "title") as? String ?? "", strCategory: dicDetails.value(forKey: "category") as? String ?? "", strPicture: dicDetails.value(forKey: "picture") as? String ?? "", strRecipe: dicDetails.value(forKey: "recipe") as? String ?? "", strSubmittedBy: dicDetails.value(forKey: "submittedby") as? String ?? "", type: dicDetails.value(forKey: "") as? String ?? "")
            btnFav.setTitle("Unfavorite", for: .normal)
            btnFav.setImage(UIImage(named: "icstar1"), for: .normal)
            
        }
        
    }
    
    
    @IBAction func clickedShare(_ sender: Any) {
        
        let url = URL(string: "https://apps.apple.com/us/app/id1554830684")!
        
        let strIntro = dicDetails.value(forKey: "recipe") as? String
        let title = dicDetails.value(forKey: "title") as? String
        
        let strDetails = "Recipe name:- \(title ?? "")\nInstruction:- \n\(strIntro ?? "")\n\n"
        
        let vc = UIActivityViewController(activityItems: [strDetails, url], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        self.present(vc, animated: true)
        
    }
    
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        
    }
    
    //MARK:- UITableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrIntro.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TBLVIEW.dequeueReusableCell(withIdentifier: "DetailsScreenTableCell") as! DetailsScreenTableCell
        
        let dicData = arrIntro[indexPath.row]
        
        cell.lblTitle.text = dicData as? String
        
        cell.lblNumber.text = "\(indexPath.row+1)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension Date {
    
    func getWeekDates() -> (thisWeek:[Date],nextWeek:[Date]) {
        var tuple: (thisWeek:[Date],nextWeek:[Date])
        var arrThisWeek: [Date] = []
        for i in 0..<7 {
            arrThisWeek.append(Calendar.current.date(byAdding: .day, value: i, to: startOfWeek)!)
        }
        var arrNextWeek: [Date] = []
        for i in 1...7 {
            arrNextWeek.append(Calendar.current.date(byAdding: .day, value: i, to: arrThisWeek.last!)!)
        }
        tuple = (thisWeek: arrThisWeek,nextWeek: arrNextWeek)
        return tuple
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var startOfWeek: Date {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: 1, to: sunday!)!
    }
    
    func toDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
