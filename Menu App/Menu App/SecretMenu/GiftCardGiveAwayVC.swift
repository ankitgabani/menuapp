//
//  GiftCardGiveAwayVC.swift
//  Menu App
//
//  Created by Gabani King on 22/07/21.
//

import UIKit
import Toast_Swift

extension Notification.Name {
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}

class GiftCardGiveAwayVC: UIViewController,UNUserNotificationCenterDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var switchOnOff: UISwitch!
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
               
                let isOnNotification = UserDefaults.standard.value(forKey: "isOnNotification") as? Bool
                
                if isOnNotification == true
                {
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = true
                    }
                }
                else  if isOnNotification == nil
                {
                    DispatchQueue.main.async {
                        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.setUpLocalNotification(hour: 9, minute: 0)
                    }
                    
                    
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = true
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = false
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    self.switchOnOff.isOn = false
                    
                    UserDefaults.standard.setValue(false, forKey: "isOnNotification")
                    UserDefaults.standard.synchronize()
                    
                }
            }
        }
        
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
                
                let isOnNotification = UserDefaults.standard.value(forKey: "isOnNotification") as? Bool
                
                if isOnNotification == true
                {
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = true
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.setUpLocalNotification(hour: 9, minute: 0)
                    }
                    
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = false
                        
                    }
                }
                
            }
            else {
                DispatchQueue.main.async {
                    self.switchOnOff.isOn = false
                    
                    UserDefaults.standard.setValue(false, forKey: "isOnNotification")
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if self.presentingViewController?.presentedViewController == self
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home: TabViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            let homeNavigation = UINavigationController(rootViewController: home)
            homeNavigation.navigationBar.isHidden = true
            appDelegate.window?.rootViewController = homeNavigation
            appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func clickedTC(_ sender: Any) {
        if let url = URL(string: TERMS_OF_CONDITION) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func clickedSwicthOnOff(_ sender: UISwitch) {
        
        if sender.isOn == true
        {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                    // Already authorized
                    
                    DispatchQueue.main.async {
                        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.setUpLocalNotification(hour: 9, minute: 0)
                    }
                    
                    UserDefaults.standard.setValue(true, forKey: "isOnNotification")
                    UserDefaults.standard.synchronize()
                }
                else {
                    // Either denied or notDetermined
                    DispatchQueue.main.async {
                        self.switchOnOff.isOn = false
                    }
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                        (granted, error) in
                        // add your own
                        UNUserNotificationCenter.current().delegate = self
                        let alertController = UIAlertController(title: "Unable to use notifications", message: "To enable notifications, go to Settings and enable notifications for this app.", preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                })
                            }
                        }
                        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                        alertController.addAction(cancelAction)
                        alertController.addAction(settingsAction)
                        DispatchQueue.main.async {
                            self.appDelegate?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            }
            
        }
        else
        {
            UIApplication.shared.cancelAllLocalNotifications()
            UserDefaults.standard.setValue(false, forKey: "isOnNotification")
            UserDefaults.standard.synchronize()
        }
    }
    
    @IBAction func clickedCheckInEmai(_ sender: Any) {
        
        if txtEmail.text == ""
        {
            self.view.makeToast("Please enter email address")
        }
        else if isValidEmail(testStr: txtEmail.text ?? "") == false
        {
            self.view.makeToast("Please enter valid email address")
        }
        else
        {
            self.view.makeToast("Successfully checked")
        }
        
    }
    
    
}
