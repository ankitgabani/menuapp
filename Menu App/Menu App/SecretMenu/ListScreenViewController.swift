//
//  ListScreenViewController.swift
//  Menu App
//
//  Created by Gabani King on 20/07/21.
//

import UIKit
import SDWebImage

class ListScreenViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearchBar: UITextField!
    
    @IBOutlet var viewBannerBase : UIView!
    @IBOutlet var consBannerHeight : NSLayoutConstraint!
    
    var objTitle = ""
    var selectedIndex = -1
    
    var arrMenu = NSMutableArray()
    var arrMenuSearching = NSMutableArray()
    
    var isSearching: Bool = false
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.current.hasNotch
        {
            if #available(iOS 11.0, *) {
                let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
                print(bottom)
                consBannerHeight.constant = 50.0 + bottom
            }
        }
        else
        {
            consBannerHeight.constant = 50
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        lblTitle.text = objTitle
        
        if selectedIndex == 0
        {
            arrMenu = (getMenuData().mutableCopy() as? NSMutableArray)!
        }
        else
        {
            let predicate = NSPredicate.init(format: "category contains[c] %@", objTitle)
            self.arrMenu = (getMenuData().filtered(using: predicate) as NSArray).mutableCopy() as! NSMutableArray
            self.tblView.reloadData()
        }
        
        
        txtSearchBar.delegate = self
        self.txtSearchBar.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height - 25
            tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
            
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: { self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) })
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if textfield.text == "" {
            self.isSearching = false
            self.tblView.reloadData()
        }
        else
        {
            self.isSearching = true
            
            self.arrMenuSearching.removeAllObjects()
            
            for i in 0..<arrMenu.count {
                
                let listItem : NSDictionary = self.arrMenu[i] as! NSDictionary
                if (listItem.value(forKey: "title") as? String)?.lowercased().range(of: self.txtSearchBar.text!.lowercased()) != nil {
                    self.arrMenuSearching.add(listItem)
                }
                
            }
            
            self.tblView.reloadData()
        }
        
    }
    
    //MARK:- Action Method
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        
    }
    
    //MARK:- UITableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return self.arrMenuSearching.count
        }else {
            return self.arrMenu.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecretMenuCell") as! SecretMenuCell
        
        var dicData = NSDictionary()
        
        if self.isSearching == true {
            dicData = arrMenuSearching[indexPath.row] as! NSDictionary
        }else {
            dicData = arrMenu[indexPath.row] as! NSDictionary
        }
        
        let title = dicData.value(forKey: "title") as? String
        cell.lblMenu.text = title
        
        cell.imgMenu.backgroundColor = UIColor.groupTableViewBackground

        var picture = dicData.value(forKey: "picture") as? String
        picture = picture?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "https://appcodism.com/jamesapps/menu/\(picture ?? "")")
        cell.imgMenu.sd_imageIndicator = SDWebImageActivityIndicator.white
        cell.imgMenu.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dicData = NSDictionary()
        
        if self.isSearching == true {
            dicData = arrMenuSearching[indexPath.row] as! NSDictionary
        }else {
            dicData = arrMenu[indexPath.row] as! NSDictionary
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailScreenViewController") as! DetailScreenViewController
        vc.dicDetails = dicData
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}

extension UIDevice {
    var hasNotch: Bool
    {
        if #available(iOS 11.0, *)
        {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else
        {
            // Fallback on earlier versions
            return false
        }
    }
}
