//
//  SecretMenuVC.swift
//  Menu App
//
//  Created by Gabani King on 19/07/21.
//

import UIKit

class SecretMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK- IBOutlet
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    @IBOutlet weak var viewAdd: UIView!
    
    
    var arrName = ["All Drinks","Cold Drinks","Frappuccinos","Hot Drinks","Teas","Drink of the Week!","$100 Gift card Giveaway!"]
    
    var arrIamge = ["icalldrinks","iccloddrinks","icfrappuccinos","ichotdrinks","icteas","icdrinkoftheweek","icgiveaway"]
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false

        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
               
                let isOnNotification = UserDefaults.standard.value(forKey: "isOnNotification") as? Bool
                
                if isOnNotification == nil
                {
                    DispatchQueue.main.async {
                        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.setUpLocalNotification(hour: 9, minute: 0)
                    }
                }
            }
        }
        
    }
    
    //MARK:- Action Method
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
    }
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedAdd(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubmitDrinkViewController") as! SubmitDrinkViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- UITableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecretMenuCell") as! SecretMenuCell
        
        cell.lblMenu.text = arrName[indexPath.row]
        cell.imgMenu.image = UIImage(named: arrIamge[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row <= 4
        {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListScreenViewController") as! ListScreenViewController
            vc.objTitle = arrName[indexPath.row]
            vc.selectedIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 5
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DrinkWaterViewController") as! DrinkWaterViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "GiftCardGiveAwayVC") as! GiftCardGiveAwayVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
