//
//  SubmitDrinkViewController.swift
//  Menu App
//
//  Created by Gabani King on 22/07/21.
//

import UIKit
import MessageUI

class SubmitDrinkViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var viewRightAcc: UIView!
    @IBOutlet var btnRightAcc: UIButton!
    
    @IBOutlet var viewLeftAcc: UIView!
    @IBOutlet var btnLeftAcc: UIButton!
    
    @IBOutlet weak var btnEmailUs: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblEmail.text = "Email us at \(SUBMIT_DRINK_EMAIL)"

        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnLeftAccClicked(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnRightAccClicked(_ sender : UIButton)
    {
        
    }
    
    @IBAction func clickedEmailUs(_ sender: Any) {
        
        let str = "Your Name(For Credit):</br>Drink Name:</br>Drink Recipe (Home to Order):</br>Don't forget to attach a Picture!"

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([SUBMIT_DRINK_EMAIL])
            mail.setMessageBody(str, isHTML: true)
            mail.setSubject("Drink Submission")
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
